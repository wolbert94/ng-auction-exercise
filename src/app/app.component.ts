import { Component, OnInit } from '@angular/core';
import { ProductItem } from './main/model/ProductItem';
import productsJson from './main/jsondb/Products.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'ng-auction-exercise';

  products:ProductItem[] = []

  filteredProducts:ProductItem[] = []   
  

  constructor() { 
    this.products = productsJson.products;
    this.filteredProducts = productsJson.products;
  }

  searchProduct(productFilter:ProductItem):void{
    this.filteredProducts = this.products.filter((f) => 
      (productFilter.name === "" || f.name === productFilter.name ) && 
      (productFilter.price === Number("") || f.price === productFilter.price ) && 
      (productFilter.category === "" || f.category === productFilter.category ));


      console.log(Math.round(4.87));
  }
}
