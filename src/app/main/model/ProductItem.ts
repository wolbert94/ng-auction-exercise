import { Star } from "./Star";

export class ProductItem{
    name:string
    price:number
    description:string
    rate:number
    category:string

    /**
     *
     */
    constructor(name:string,price:number,description:string,rate:number,category:string) {
        this.name=name;
        this.price=price;
        this.description=description;
        this.rate=rate;
        this.category=category;
    }
}