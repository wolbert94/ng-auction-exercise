import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductItem } from '../../model/ProductItem';

@Component({
  selector: 'ng-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss']
})
export class ProductSearchComponent {

  @Output()
  productItemsSearch = new EventEmitter<ProductItem>();

  @Input()
  products: ProductItem[] = [];

  categories = [{ value: 0, name: "All sections" },
  { value: 0, name: "Informatics" },
  { value: 0, name: "Electronics" },
  { value: 0, name: "Sports" },
  { value: 0, name: "Workout" }];

  constructor() { }

  searchProduct(title: HTMLInputElement, price: HTMLInputElement, category: HTMLSelectElement) {

    let cat = ""
    if (category.selectedIndex !== 0) {
      cat = category[category.selectedIndex].innerText;
    }

    let productFilter = new ProductItem(title.value, Number(price.value), "", 0, cat);

    this.productItemsSearch.emit(productFilter);
  }
}
